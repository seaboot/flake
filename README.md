# seaboot-flack

业务需求：制作报表系统。

我们思考到一件事情，需要报表系统的公司，一般编码的水平也很高。

制作报表系统的时候，与其设计一些很新的东西，搞一堆配置，去生成一条 SQL。

还不如直接开放写 SQL 的权限，想查什么自己来就是了。

#### 核心功能

我们模仿了 mybatis 渲染 sql 语句的方式，核心用法如下。

```java
class Test{

    public static void main(String[] args) {
        // 一条带模版语法的 sql
        Mapper mapper = new Mapper();
        String sql =
                "select * from table where id = @{id} " +
                "<% if(name != null && name != ''){%> and name = #{name} <%}%>" +
                "<% if(age >= 18){%> and age > @{age} <%}%>";
        mapper.setTemplate(sql);

        // 参数
        Map<String, Object> params = new HashMap<>();
        params.put("id", "1");
        params.put("name", null);
        params.put("age", 18);

        // 渲染 sql，产生的结果，可直接对接 jdbc
        Flake flake = new Flake();
        PreparedSql preparedSql = flake.processSql(mapper, params);
        System.out.println(preparedSql);

        // 执行一个查询
        Object ret = flake.query(preparedSql);
        System.out.println(ret);
    }
}
```

在最新版本中，我们删除了多余的设计，只保留了最核心的功能；

允许开发人员，发挥自己的想象，按照自己的想法，组织业务逻辑。

#### 实现原理

参考了 mybatis 的实现方式：

1. 首先，处理SQL模版中的逻辑运算（if语句等），渲染出基本的SQL语句；
2. 然后，替换语句中的所有@{}占位符；
3. 最后，通过JDBC执行解析好的查询语句。

底层技术实现：

1. JDBC部分，使用JdbcTemplate；
2. SQL渲染部分使用Freemarker，允许替换为其它模版引擎；
3. ${}是通过模版引擎实现，会被直接渲染；
4. @{}会被问号替换，这一部分通过分词器实现，允许重新定义这一部分代码。

（占位符为什么是@{}而不是#{}？ 因为#{}也是大部分模版引擎默认使用的占位符，容易造成冲突）

#### 版本

1. 2022-02-10：项目启动
2. 2022-02-20：代码版本1.0
3. 2022-02-28：增减表单/查询页面数据配置
4. 2022-10-27：增加大量代码注释，优化API，代码更容易理解
5. 2024-05-13：引入 beetl，语法用起来会更加舒服一些


