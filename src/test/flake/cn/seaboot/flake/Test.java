package cn.seaboot.flake;

import cn.seaboot.flake.core.Flake;
import cn.seaboot.flake.core.PreparedSql;
import cn.seaboot.flake.mapping.Mapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr.css
 * @version 2022-01-27 10:48
 */
public class Test {



    /**
     * 构造一个用于测试的DataSource
     * <p>
     * DataSource dataSource = createSimpleDataSource(
     * "com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/med", "root", "root"
     * );
     *
     * @param driverClass 驱动
     * @param url         地址
     * @param userName    用户名
     * @param passWord    密码
     * @return DataSource
     */
    public static DataSource createSimpleDataSource(String driverClass, String url, String userName, String passWord) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        dataSource.setPassword(passWord);
        return dataSource;
    }

    public static void main(String[] args) {
        String[] arr = "13,1,4;5".split("[,|;]");
        System.out.println(Arrays.toString(arr));

//        // 一条带模版语法的SQL
//        Mapper mapper = new Mapper();
//        String sql =
//                "select * from table where id = @{id} " +
//                "<% if(name != null && name != ''){%> and name = #{name} <%}%>" +
//                "<% if(age >= 18){%> and age > @{age} <%}%>";
//        mapper.setTemplate(sql);
//
//        //参数
//        Map<String, Object> params = new HashMap<>();
//        params.put("id", "1");
//        params.put("name", null);
//        params.put("age", 18);
//
//        //渲染SQL
//        Flake flake = new Flake();
//        PreparedSql preparedSql = flake.processSql(mapper, params);
//        System.out.println(preparedSql);
//
//        //执行一个查询
//        Object ret = flake.query(preparedSql);
//        System.out.println(ret);
    }

}
