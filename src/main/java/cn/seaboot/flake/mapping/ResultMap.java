package cn.seaboot.flake.mapping;

import java.io.Serializable;
import java.util.List;

/**
 * @author Mr.css
 * @version 2022-01-28 15:32
 */
public class ResultMap implements Serializable {
    private static final long serialVersionUID = -8572131578376373123L;
    /**
     * 唯一ID，针对所有ParameterMap有效
     */
    private String id;
    /**
     * 全类名
     */
    private Class<?> type;
    /**
     * 字段明细
     */
    private List<ResultMapping> resultMappings;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public List<ResultMapping> getResultMappings() {
        return resultMappings;
    }

    public void setResultMappings(List<ResultMapping> resultMappings) {
        this.resultMappings = resultMappings;
    }
}
