package cn.seaboot.flake.mapping;

import org.apache.ibatis.type.JdbcType;

import java.io.Serializable;

/**
 * 结果集配置，未来可以尝试加入数据字典等配置，自动匹配前端界面
 *
 * @author Mr.css
 * @version 2022-01-28 15:32
 */
public class ResultMapping implements Serializable {
    private static final long serialVersionUID = 3158138717403857107L;
    /**
     * 字段名
     */
    private String property;
    /**
     * 列名
     */
    private String column;
    /**
     * 字段类型
     */
    private Class<?> javaType;
    /**
     * jdbc类型
     */
    private JdbcType jdbcType;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Class<?> getJavaType() {
        return javaType;
    }

    public void setJavaType(Class<?> javaType) {
        this.javaType = javaType;
    }

    public JdbcType getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(JdbcType jdbcType) {
        this.jdbcType = jdbcType;
    }

    @Override
    public String toString() {
        return "ResultMapping{" +
                "property='" + property + '\'' +
                ", column='" + column + '\'' +
                ", javaType=" + javaType +
                ", jdbcType=" + jdbcType +
                '}';
    }
}
