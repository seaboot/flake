package cn.seaboot.flake.mapping;

import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.type.JdbcType;

import java.io.Serializable;

/**
 * 与mybatis同名对象存在差异，去除了mybatis配置相关的数据，主要用于逆向分析代码
 *
 * @author Mr.css
 * @version 2022-01-28 10:52
 */
public class ParameterMapping implements Serializable {
    private static final long serialVersionUID = -2255126962224391888L;
    /**
     * 列名
     */
    private String property;
    /**
     * 参数类型
     */
    private ParameterMode mode;
    /**
     * 字段类型
     */
    private Class<?> javaType;
    /**
     * jdbc类型
     */
    private JdbcType jdbcType;
    /**
     * 小数点精度
     */
    private Integer numericScale;
    /**
     * 参数表达式
     */
    private String expression;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public ParameterMode getMode() {
        return mode;
    }

    public void setMode(ParameterMode mode) {
        this.mode = mode;
    }

    public Class<?> getJavaType() {
        return javaType;
    }

    public void setJavaType(Class<?> javaType) {
        this.javaType = javaType;
    }

    public JdbcType getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(JdbcType jdbcType) {
        this.jdbcType = jdbcType;
    }

    public Integer getNumericScale() {
        return numericScale;
    }

    public void setNumericScale(Integer numericScale) {
        this.numericScale = numericScale;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public String toString() {
        return "ParameterMapping{" +
                "property='" + property + '\'' +
                ", mode=" + mode +
                ", javaType=" + javaType +
                ", jdbcType=" + jdbcType +
                ", numericScale=" + numericScale +
                ", expression='" + expression + '\'' +
                '}';
    }
}
