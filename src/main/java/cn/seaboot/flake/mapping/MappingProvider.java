package cn.seaboot.flake.mapping;

import java.util.List;

/**
 * 数据库映射配置查询
 * <p>
 *
 * @author Mr.css
 * @version 2022-02-15 16:18
 */
public interface MappingProvider {

    /**
     * 返回完整的{#link Mapper}数据信息，业务上不再验证字段是否为空
     *
     * @param mapperGroup 映射分组
     * @param id          配置ID
     * @return 映射信息
     */
    Mapper queryMapperById(String mapperGroup, String id);

    /**
     * 返回完整的参数映射，字段信息必须完整{@link ParameterMap#getParameterMappings()}
     *
     * @param parameterId 参数类型
     * @return 参数映射
     */
    ParameterMap queryParameterMapById(String parameterId);

    /**
     * 返回参数映射明细
     *
     * @param parameterId 参数类型
     * @return 参数映射
     */
    List<ParameterMapping> queryParameterMappingById(String parameterId);

    /**
     * 返回完整的结果映射，字段信息必须完整{@link ResultMap#getResultMappings()}
     *
     * @param resultId 结果类型
     * @return 结果映射
     */
    ResultMap queryResultMapById(String resultId);

    /**
     * 返回完整的结果映射
     *
     * @param resultId 结果类型
     * @return 结果映射
     */
    List<ResultMapping> queryResultMappingById(String resultId);
}
