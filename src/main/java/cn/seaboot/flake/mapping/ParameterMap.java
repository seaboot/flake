package cn.seaboot.flake.mapping;

import java.io.Serializable;
import java.util.List;

/**
 * 与mybatis存在差异，去除了mybatis配置相关的数据，主要用于逆向分析代码，
 * 1、加入参数校验规则，因为已经脱离了常规代码，hibernate-validate在我们的代码中不适用，
 * 2、加入表单控件配置，根据配置，直接生成表单。
 *
 * @author Mr.css
 * @version 2022-01-28 10:52
 */
public class ParameterMap implements Serializable {
    private static final long serialVersionUID = 7228517767140935320L;
    /**
     * 唯一ID，针对所有ParameterMap有效
     */
    private String id;
    /**
     * 全类名
     */
    private Class<?> type;
    /**
     * 字段明细
     */
    private List<ParameterMapping> parameterMappings;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void setParameterMappings(List<ParameterMapping> parameterMappings) {
        this.parameterMappings = parameterMappings;
    }

    @Override
    public String toString() {
        return "ParameterMap{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", parameterMappings=" + parameterMappings +
                '}';
    }
}
