package cn.seaboot.flake.mapping;

import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.StatementType;

import java.io.Serializable;

/**
 * 与mybatis中update、delete、insert标签功能一致
 *
 * @author Mr.css
 * @version 2022-04-28 15:06
 */
public class Mapper implements Serializable {
    private static final long serialVersionUID = -3077016688015132127L;

    /**
     * 唯一ID，相对于database_id保证唯一，
     * 如果需要制作模版缓存，id可以用于制作Cache的key值。
     */
    private String id;
    /**
     * 分组ID
     */
    private String mapperGroup;
    /**
     * 数据库ID
     */
    private String databaseId;
    /**
     * SQL模版
     */
    private String template;
    /**
     * 参数类型
     */
    private String parameterType;
    /**
     * 返回值类型
     */
    private String resultType;
    /**
     * 类型参考：{@link StatementType}
     */
    private String statementType;
    /**
     * 类型参考：{@link SqlCommandType}
     */
    private String commandType;
    /**
     * 参数集配置
     */
    private ParameterMap parameterMap;
    /**
     * 结果集配置
     */
    private ResultMap resultMap;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMapperGroup() {
        return mapperGroup;
    }

    public void setMapperGroup(String mapperGroup) {
        this.mapperGroup = mapperGroup;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public String getStatementType() {
        return statementType;
    }

    public void setStatementType(String statementType) {
        this.statementType = statementType;
    }

    public String getCommandType() {
        return commandType;
    }

    public void setCommandType(String commandType) {
        this.commandType = commandType;
    }

    public ParameterMap getParameterMap() {
        return parameterMap;
    }

    public void setParameterMap(ParameterMap parameterMap) {
        this.parameterMap = parameterMap;
    }

    public ResultMap getResultMap() {
        return resultMap;
    }

    public void setResultMap(ResultMap resultMap) {
        this.resultMap = resultMap;
    }

    @Override
    public String toString() {
        return "Mapper{" +
                "id='" + id + '\'' +
                ", mapperGroup='" + mapperGroup + '\'' +
                ", databaseId='" + databaseId + '\'' +
                ", template='" + template + '\'' +
                ", parameterType='" + parameterType + '\'' +
                ", parameterMap='" + parameterMap + '\'' +
                ", resultType='" + resultType + '\'' +
                ", statementType='" + statementType + '\'' +
                ", commandType='" + commandType + '\'' +
                ", resultMap=" + resultMap +
                '}';
    }
}
