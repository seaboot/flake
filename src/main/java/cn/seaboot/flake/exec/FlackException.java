package cn.seaboot.flake.exec;

/**
 * 目前没做什么异常区分
 *
 * @author Mr.css on 2017-8-21
 */
public class FlackException extends RuntimeException {
    private static final long serialVersionUID = -6346711359550249176L;

    public FlackException() {
    }

    public FlackException(String message) {
        super(message);
    }

    public FlackException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlackException(Throwable cause) {
        super(cause);
    }
}
