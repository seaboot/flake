package cn.seaboot.flake.jdbc;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 批量设置ID，主要用于deleteByIds()这一类函数的封装
 *
 * @author Mr.css
 * @version 2021-01-08 10:20
 */
public class BatchIdSetter implements BatchPreparedStatementSetter {

    private final Object[] ids;

    public BatchIdSetter(Object[] ids) {
        this.ids = ids;
    }

    @Override
    public void setValues(PreparedStatement ps, int i) throws SQLException {
        ps.setObject(1, ids[i]);
    }

    @Override
    public int getBatchSize() {
        return ids.length;
    }
}
