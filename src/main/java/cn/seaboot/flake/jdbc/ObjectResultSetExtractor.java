package cn.seaboot.flake.jdbc;

import cn.seaboot.commons.core.StringUtils;
import cn.seaboot.commons.reflect.FieldAccess;
import cn.seaboot.commons.reflect.ObjectField;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * 将查询到的数据打包成对象
 *
 * @author Mr.css
 * @version 2020-11-15 10:41
 */
public class ObjectResultSetExtractor<T> implements ResultSetExtractor<T> {
    private final Class<T> clazz;

    public ObjectResultSetExtractor(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        if (resultSet.next()) {
            FieldAccess fieldAccess = FieldAccess.create(clazz);
            T obj = fieldAccess.newInstance();
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1, len = metaData.getColumnCount() + 1; i < len; i++) {
                String name = StringUtils.underlineToCamel(metaData.getColumnLabel(i));
                ObjectField field = fieldAccess.getField(name);
                if (field != null) {
                    field.setValue(obj, resultSet.getObject(i));
                }
            }
            return obj;
        }
        return null;
    }
}
