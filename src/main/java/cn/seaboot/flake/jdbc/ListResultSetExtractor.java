package cn.seaboot.flake.jdbc;

import cn.seaboot.commons.core.StringUtils;
import cn.seaboot.commons.reflect.FieldAccess;
import cn.seaboot.commons.reflect.ObjectField;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 将查询到的数据打包成对象
 *
 * @author Mr.css
 * @version 2020-11-15 10:41
 */
public class ListResultSetExtractor<T> implements ResultSetExtractor<List<T>> {
    private final Class<T> clazz;

    public ListResultSetExtractor(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public List<T> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<T> list = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        FieldAccess fieldAccess = FieldAccess.create(clazz);
        int len = metaData.getColumnCount() + 1;
        ObjectField[] names = new ObjectField[len];
        for (int i = 1; i < len; i++) {
            String name = StringUtils.underlineToCamel(metaData.getColumnName(i));
            names[i] = fieldAccess.getField(name);
        }
        while (resultSet.next()) {
            T obj = fieldAccess.newInstance();
            for (int i = 1; i < len; i++) {
                ObjectField field = names[i];
                if (field != null) {
                    field.setValue(obj, resultSet.getObject(i));
                }
            }
            list.add(obj);
        }
        return list;
    }
}
