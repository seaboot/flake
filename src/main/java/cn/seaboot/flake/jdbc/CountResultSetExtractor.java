package cn.seaboot.flake.jdbc;

import cn.seaboot.commons.core.Converter;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 做求和运算时的返回值封装。
 * ResultSetExtractor for 'query the count or sum for data'
 *
 * @author Mr.css
 * @version 2020-11-15 10:41
 */
public class CountResultSetExtractor implements ResultSetExtractor<Number> {

    /**
     * 从resultSet获取查询结果，只会获取第一个值，即便查询结果存在其它值
     *
     * @param resultSet 结果集
     * @return value or null
     * @throws SQLException        -
     * @throws DataAccessException -
     * @throws ClassCastException  query result set can not case to number
     */
    @Override
    public Number extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        if (resultSet.next()) {
            Object ret = resultSet.getObject(1);
            if(ret instanceof Number){
                return (Number) ret;
            } else {
                return Converter.toDouble(ret);
            }
        } else {
            return null;
        }
    }
}
