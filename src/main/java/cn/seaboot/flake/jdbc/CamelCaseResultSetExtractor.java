package cn.seaboot.flake.jdbc;

import cn.seaboot.commons.core.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 将数据库查询的数据封装成Map，但是key值从下划线命名，转为驼峰式命名
 *
 * @author Mr.css
 * @version 2020-11-15 10:41
 */
public class CamelCaseResultSetExtractor<T> implements ResultSetExtractor<List<Map<String, Object>>> {

    @Override
    public List<Map<String, Object>> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List<Map<String, Object>> list = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int len = metaData.getColumnCount() + 1;
        String[] names = new String[len];
        for (int i = 1; i < len; i++) {
            names[i] = StringUtils.underlineToCamel(metaData.getColumnName(i));
        }
        while (resultSet.next()) {
            Map<String, Object> map = new HashMap<>();
            for (int i = 1; i < len; i++) {
                map.put(names[i], resultSet.getObject(i));
            }
            list.add(map);
        }
        return list;
    }
}
