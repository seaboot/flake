package cn.seaboot.flake.jdbc;

import cn.seaboot.commons.lang.Warning;
import cn.seaboot.commons.reflect.FieldAccess;
import cn.seaboot.flake.exec.FlackException;
import cn.seaboot.flake.mapping.ResultMap;
import cn.seaboot.flake.mapping.ResultMapping;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 需要提前扫描结果集的映射配置，依赖于{@link ResultMap}
 *
 * @author Mr.css
 * @version 2020-11-15 10:41
 */
public class FlackResultSetExtractor<T> implements ResultSetExtractor<List<T>> {
    private final Class<?> clazz;
    private final List<ResultMapping> resultMappings;
    private final FieldAccess fieldAccess;

    public FlackResultSetExtractor(ResultMap resultMap) {
        this.clazz = resultMap.getType();
        this.resultMappings = resultMap.getResultMappings();
        this.fieldAccess = FieldAccess.create(clazz);
    }

    @Override
    @SuppressWarnings({Warning.UNCHECKED, Warning.RAW_TYPES})
    public List<T> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        List list = new ArrayList();
        while (resultSet.next()) {
            try {
                Object obj = clazz.newInstance();
                for (ResultMapping resultMapping : resultMappings) {
                    Object ret = resultSet.getObject(resultMapping.getColumn());
                    fieldAccess.setValue(obj, resultMapping.getProperty(), ret);
                }
                list.add(obj);
            } catch (InstantiationException | IllegalAccessException e) {
                throw new FlackException("can not extract resultSet!", e);
            }
        }
        return (List<T>) list;
    }
}
