package cn.seaboot.flake.core;

import cn.seaboot.commons.lang.P;

import java.sql.Statement;
import java.util.Arrays;

/**
 * 预处理好的 SQL 脚本以及参数，可以直接在 JDBC 上运行
 *
 * @author Mr.css
 * @version 2022-01-26 17:01
 */
public class PreparedSql {
    /**
     * 能被{@link Statement}执行的SQL
     */
    private String sql;
    /**
     * 参数数组，已经根据占位符做好排序
     */
    private Object[] values;

    public PreparedSql(String sql, Object[] values) {
        this.sql = sql;
        this.values = values;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "SQL         ===>: " + sql + P.lineSeparator() +
                "Parameters  ===>: " + Arrays.toString(values);
    }
}
