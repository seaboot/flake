package cn.seaboot.flake.core;

import cn.seaboot.commons.file.Beetles;
import cn.seaboot.flake.mapping.Mapper;

import java.util.Map;

/**
 * SQL 模版解析程序
 * <p>
 * 用于处理代码中的逻辑判断，最后得出带有占位符的SQL
 *
 * @author Mr.css
 * @version 2022-01-28 10:16
 */
public class BeetlesTemplate implements FlakeTemplate {

    /**
     * 完成所有函数运算，只保留函数的占位符
     *
     * @param mapper SQL模型
     * @param params 参数
     * @return 去除逻辑运算之后的SQL
     */
    @Override
    public String process(Mapper mapper, Map<String, Object> params) {
        return Beetles.process(mapper.getTemplate(), params);
    }
}
