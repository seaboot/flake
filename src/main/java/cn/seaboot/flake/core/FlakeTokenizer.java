package cn.seaboot.flake.core;

import java.util.Map;

/**
 * SQL分词器
 *
 * 使用 '?' 替换所有占位符，最终返回{@link PreparedSql}。
 *
 * @author Mr.css
 * @version 2022-01-27 9:57
 */
public interface FlakeTokenizer {

    /**
     * 预处理SQL
     *
     * @param tmp    代码模版
     * @param params 参数
     * @return 预置SQL
     */
    PreparedSql process(String tmp, Map<String, Object> params);
}
