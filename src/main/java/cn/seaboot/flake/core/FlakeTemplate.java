package cn.seaboot.flake.core;

import cn.seaboot.flake.mapping.Mapper;

import java.util.Map;

/**
 * SQL 模版解析程序
 *
 * 用于处理代码中的逻辑判断，最后得出带有占位符的SQL
 *
 * @author Mr.css
 * @version 2022-01-28 10:10
 */
public interface FlakeTemplate {

    /**
     * 完成所有函数运算，只保留函数的占位符
     *
     * @param module SQL模型
     * @param params 参数
     * @return 语句模版，E.G.: SELECT * FROM TABLE WHERE ID = @{ID}
     */
    String process(Mapper module, Map<String, Object> params);
}
